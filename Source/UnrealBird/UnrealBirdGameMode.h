// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "GameFramework/GameMode.h"
#include "UnrealBirdGameMode.generated.h"

// The GameMode defines the game being played. It governs the game rules, scoring, what actors
// are allowed to exist in this game type, and who may enter the game.
//
// This game mode just sets the default pawn to be the MyCharacter asset, which is a subclass of UnrealBirdCharacter

UCLASS(minimalapi)
class AUnrealBirdGameMode : public AGameMode
{
	GENERATED_BODY()
public:
	AUnrealBirdGameMode();

	bool isGameRunning = false;
	UFUNCTION(BlueprintCallable, Category = "GameState")
		void StartGame();
	UFUNCTION(BlueprintCallable, Category = "GameState")
		void EndGame();
	UFUNCTION(BlueprintCallable, Category = "GameState")
		FORCEINLINE bool IsGameRunning() const { return isGameRunning; }
};
