// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "UnrealBird.h"
#include "UnrealBirdGameMode.h"
#include "UnrealBirdCharacter.h"

AUnrealBirdGameMode::AUnrealBirdGameMode()
{
	// set default pawn class to our character
	DefaultPawnClass = AUnrealBirdCharacter::StaticClass();	
}

void AUnrealBirdGameMode::StartGame()
{
	isGameRunning = true;
}

void AUnrealBirdGameMode::EndGame()
{
	isGameRunning = false;
}
