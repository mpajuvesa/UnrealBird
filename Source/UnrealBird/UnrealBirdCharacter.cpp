// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "UnrealBird.h"
#include "UnrealBirdCharacter.h"
#include "PaperFlipbookComponent.h"
#include "Components/TextRenderComponent.h"

#define print(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 1.5, FColor::White,text)

DEFINE_LOG_CATEGORY_STATIC(SideScrollerCharacter, Log, All);
//////////////////////////////////////////////////////////////////////////
// AUnrealBirdCharacter

AUnrealBirdCharacter::AUnrealBirdCharacter()
{

	PlayerMoving = MovementState::Waiting;	
	// Use only Yaw from the controller and ignore the rest of the rotation.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	// Set the size of our collision capsule.
	GetCapsuleComponent()->SetCapsuleHalfHeight(96.0f);
	GetCapsuleComponent()->SetCapsuleRadius(40.0f);

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 500.0f;
	CameraBoom->SocketOffset = FVector(0.0f, 0.0f, 75.0f);
	CameraBoom->bAbsoluteRotation = true;
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->RelativeRotation = FRotator(0.0f, -90.0f, 0.0f);
	

	// Create an orthographic camera (no perspective) and attach it to the boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->ProjectionMode = ECameraProjectionMode::Orthographic;
	SideViewCameraComponent->OrthoWidth = 2048.0f;
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);

	// Prevent all automatic rotation behavior on the camera, character, and camera component
	CameraBoom->bAbsoluteRotation = true;
	SideViewCameraComponent->bUsePawnControlRotation = false;
	SideViewCameraComponent->bAutoActivate = true;
	GetCharacterMovement()->bOrientRotationToMovement = false;

	// Configure character movement
	GetCharacterMovement()->GravityScale = 2.0f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.0f;
	GetCharacterMovement()->MaxWalkSpeed = 600.0f;
	GetCharacterMovement()->MaxFlySpeed = 600.0f;

	// Lock character motion onto the XZ plane, so the character can't move in or out of the screen
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->SetPlaneConstraintNormal(FVector(0.0f, -1.0f, 0.0f));

	// Behave like a traditional 2D platformer character, with a flat bottom instead of a curved capsule bottom
	// Note: This can cause a little floating when going up inclines; you can choose the tradeoff between better
	// behavior on the edge of a ledge versus inclines by setting this to true or false
	GetCharacterMovement()->bUseFlatBaseForFloorChecks = true;

// 	TextComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("IncarGear"));
// 	TextComponent->SetRelativeScale3D(FVector(3.0f, 3.0f, 3.0f));
// 	TextComponent->SetRelativeLocation(FVector(35.0f, 5.0f, 20.0f));
// 	TextComponent->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));
// 	TextComponent->SetupAttachment(RootComponent);

	// Enable replication on the Sprite component so animations show up when networked
	GetSprite()->SetIsReplicated(true);
	bReplicates = true;
}

//////////////////////////////////////////////////////////////////////////
// Animation

void AUnrealBirdCharacter::UpdateAnimation()
{
	const FVector PlayerVelocity = GetVelocity();
	const float PlayerSpeedSqr = PlayerVelocity.SizeSquared();

	// Are we moving or standing still?
	UPaperFlipbook* DesiredAnimation = (PlayerSpeedSqr > 0.0f) ? RunningAnimation : IdleAnimation;
	if( GetSprite()->GetFlipbook() != DesiredAnimation 	)
	{
		GetSprite()->SetFlipbook(DesiredAnimation);
	}
}
void AUnrealBirdCharacter::BeginPlay() 
{
	Super::BeginPlay();
	PrimaryActorTick.bCanEverTick = true;
	MyController = GetWorld()->GetFirstPlayerController();
	MyController->bShowMouseCursor = true;
	MyController->bEnableClickEvents = true;
	MyController->bEnableMouseOverEvents = true;

	FlySpeed = 10.0;
	DropSpeed = 6.0;
	RiseSpeed = 8.0;
}
void AUnrealBirdCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (PlayerMoving != MovementState::Dead && PlayerMoving != MovementState::Waiting) {
		FVector ActorLocation;
		ActorLocation = GetActorLocation();

		switch (PlayerMoving) {
		case MovementState::Rising:
			ActorLocation.Z += RiseSpeed;
			break;
		case MovementState::Falling:
			ActorLocation.Z -= DropSpeed;
			break;
		}

		ActorLocation.X += FlySpeed;

		FHitResult HitResult;
		if (SetActorLocation(ActorLocation, true, &HitResult) == false) {
			KillBird();
		}
	}
	
	UpdateCharacter();	
}

void AUnrealBirdCharacter::MoveUp() {
	if (PlayerMoving != MovementState::Dead && PlayerMoving != MovementState::Waiting) {
		PlayerMoving = MovementState::Rising;
	}
	
}
void AUnrealBirdCharacter::MoveDown() {
	if (PlayerMoving != MovementState::Dead && PlayerMoving != MovementState::Waiting) {
		PlayerMoving = MovementState::Falling;
		
	}
}
void AUnrealBirdCharacter::StartFlying() {
	PlayerMoving = MovementState::Falling;
}
void AUnrealBirdCharacter::StopFlying() {
	PlayerMoving = MovementState::Waiting;
}
void AUnrealBirdCharacter::KillBird_Implementation()
{
	PlayerMoving = MovementState::Dead;

	GetCapsuleComponent()->SetSimulatePhysics(true);

	GetSprite()->SetPlayRate(0);


	print("Player dead");
}
//////////////////////////////////////////////////////////////////////////
// Input

void AUnrealBirdCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Note: the 'Jump' action and the 'MoveRight' axis are bound to actual keys/buttons/sticks in DefaultInput.ini (editable from Project Settings..Input)
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AUnrealBirdCharacter::MoveUp);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AUnrealBirdCharacter::MoveDown);
	//PlayerInputComponent->BindAxis("MoveRight", this, &AUnrealBirdCharacter::MoveRight);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &AUnrealBirdCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AUnrealBirdCharacter::TouchStopped);
}

//void AUnrealBirdCharacter::MoveRight(float Value)
//{
//	/*UpdateChar();*/
//
//	// Apply the input to the character motion
//	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
//}

void AUnrealBirdCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// jump on any touch
	Jump();
}

void AUnrealBirdCharacter::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	StopJumping();
}

void AUnrealBirdCharacter::UpdateCharacter()
{
	// Update animation to match the motion
	UpdateAnimation();

	// Now setup the rotation of the controller based on the direction we are travelling
	const FVector PlayerVelocity = GetVelocity();	
	float TravelDirection = PlayerVelocity.X;
	// Set the rotation so that the character faces his direction of travel.
	if (Controller != nullptr)
	{
		if (TravelDirection < 0.0f)
		{
			Controller->SetControlRotation(FRotator(0.0, 180.0f, 0.0f));
		}
		else if (TravelDirection > 0.0f)
		{
			Controller->SetControlRotation(FRotator(0.0f, 0.0f, 0.0f));
		}
	}
}
