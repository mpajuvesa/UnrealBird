// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperCharacter.h"
#include "Birdie.generated.h"

/**
 * 
 */
UCLASS()
class UNREALBIRD_API ABirdie : public APaperCharacter
{
	GENERATED_BODY()
	enum MovingState {Falling, Rising, Dead};
public:
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	ABirdie();
private:
	MovingState PlayerMovingState;
	void MoveUp();
	void MoveForward();
};
