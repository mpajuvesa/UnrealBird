// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CollisionObject.generated.h"

UCLASS()
class UNREALBIRD_API ACollisionObject : public AActor
{
	GENERATED_BODY()
	
public:	
	
	UPROPERTY(EditAnywhere)
	float speed;
	// Sets default values for this actor's properties
	ACollisionObject();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	
	
};
