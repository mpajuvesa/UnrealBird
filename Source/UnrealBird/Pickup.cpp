// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealBird.h"
#include "Pickup.h"


// Sets default values
APickup::APickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bIsActive = true;

	USphereComponent* coll = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));

	RootComponent = coll;

	Sprite = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("Sprite"));

	Sprite->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickup::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

bool APickup::IsActive()
{
	return bIsActive;
}

void APickup::SetActive(bool state)
{
	bIsActive = state;
}

void APickup::WasCollected()
{
	Destroy();
}

