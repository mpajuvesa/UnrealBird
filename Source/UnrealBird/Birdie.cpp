// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealBird.h"
#include "Birdie.h"

ABirdie::ABirdie() {
	PrimaryActorTick.bCanEverTick = true;
	PlayerMovingState = Falling;

	AutoPossessPlayer = EAutoReceiveInput::Player0;
}
void ABirdie::Tick(float DeltaSeconds) {
	if (PlayerMovingState != Rising && PlayerMovingState != Dead) {
		MoveForward();
	}
}
void ABirdie::SetupPlayerInputComponent(class UInputComponent* InputComponent) {
	InputComponent->BindAction("Up", IE_Pressed, this, &ABirdie::MoveUp);
}
void ABirdie::MoveUp() {
	if (PlayerMovingState != Dead) {
		PlayerMovingState = Rising;
	}
}
void ABirdie::MoveForward() {
	FVector ActorLocation = GetActorLocation();
	ActorLocation.X += 1.0f;
	FHitResult HitResult;
	if (SetActorLocation(ActorLocation, true, &HitResult) == false) {
		if (HitResult.GetActor() != nullptr) {
			PlayerMovingState = Dead;
		}
	}
}


