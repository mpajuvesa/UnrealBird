// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealBird.h"
#include "CollisionObject.h"


// Sets default values
ACollisionObject::ACollisionObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
}

// Called when the game starts or when spawned
void ACollisionObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACollisionObject::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	FVector vec = FVector(speed, 0.0f, 0.0f);
	AddActorLocalOffset(vec);

}

